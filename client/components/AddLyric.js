import React, { Component } from 'react';
import addLyric from '../queries/addLyric';
import { graphql } from 'react-apollo';

class AddLyric extends Component {
  constructor(props) {
    super(props);

    this.state = {content: ''};
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.mutate({
      variables: {
        songId: this.props.songId,
        content: this.state.content,
      },
    });
    this.setState({ content: ''});
  }

  render() {
    return (
      <form onSubmit={this.onSubmit.bind(this)}>
        <label>Add a lyric</label>
        <input
          value={this.state.content}
          onChange={e => this.setState({ content: e.target.value })}/>
      </form>
    );
  }
}


export default graphql(addLyric)(AddLyric);
