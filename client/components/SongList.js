import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import query from '../queries/fetchSongsList';
import deleteMutation from '../queries/deleteMutation';
import { Link } from 'react-router';

class SongList extends Component {
  onSongDelete(id) {
    this.props.mutate({ variables: { id } })
      .then(() => this.props.data.refetch());
  }

  renderSongs(songList) {
    return songList.map(({ id, title }) => {
      return (
        <li key={id} className='collection-item'>
          <Link to={`/songs/${id}`}>{title}</Link>
          <i
            className='material-icons'
            onClick={() => this.onSongDelete(id)}
          >delete</i>
        </li>
      )
    })
  }

  render() {
    if (this.props.data.loading) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <ul className='collection'>{this.renderSongs(this.props.data.songs)}</ul>
        <Link
          className={'btn-floating btn-large red right'}
          to='/songs/new'>
          <i className='material-icons'>add</i>
        </Link>
      </div>
    );
  }
}

export default graphql(deleteMutation)(
  graphql(query)(SongList)
);